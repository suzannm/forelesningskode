import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
x = np.arange(0,2,0.01)
f = x**2
g = x**3
h = np.sqrt(x)

plt.plot(x,f)
plt.plot(x,g)
plt.plot(x,h)

plt.show()